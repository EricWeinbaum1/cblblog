<?php



/*
 * Excerpt overrides
 */
add_filter('excerpt_more', function ($more) {
	return '';
});

add_filter('excerpt_length', function ($length) {
	return 32;
});

// $id = post ID, $max = max amount of words to return
function get_excerpt_clipped($id, $max) {
	$post = get_post($id);

	add_filter('excerpt_length', function() use(&$max) {
		return $max;
	});

	return apply_filters('get_the_excerpt', $post->post_excerpt);
}









/*
 * Archive pages
 */
add_filter('get_the_archive_title', function ($title) {

    if(is_archive()) {
    	$title = str_replace('Month: ', 'Posts from ', $title);
    }

    if (is_category()) {
    	$title = str_replace('Category: ', '', $title);
    }

    return $title;
});









/*
 * Admin section
 */ 

// Custom admin CSS
add_action('login_head', function() { 
	echo '
	<style type="text/css"> 
		.login h1 a {
			background-image: url(' . get_bloginfo('template_directory') . '/public/img/cbl_logo_230.png) !important;
			background-size: auto auto !important;
			width: auto !important;
			height: 46px;
		} 
		body, html {
		    background: #303D4E;
		    background-color: #303D4E;
		}
		body,html,a,div,p,input,button,a,
		.login #backtoblog a,
		.login #nav a,
		.login h1 a {
			color: #FFF;
		}
		.login label,
		.login .message,
		.login .message a,
		.login #login_error,
		.login #login_error a {
			color: #000;
		}
		.login form {
			box-shadow: 0 0 10px #000;
		}
	</style>'; 
});

// Clear footer text
add_filter('admin_footer_text', function() { 
	echo ''; 
});

// Turning off annoying update alerts
if (!current_user_can('edit_users')) {
	add_action('init', create_function('$a', "remove_action('init', 'wp_version_check');"), 2);
	add_filter('pre_option_update_core', create_function('$a', "return null;"));
}

// Remove unnecessary pages
add_action('admin_menu', function() {
	remove_menu_page('edit-comments.php');
	remove_menu_page('themes.php');
	remove_menu_page('plugins.php');
	remove_menu_page('tools.php');
	remove_menu_page('edit.php?post_type=page');
});




// Removing unnecessary dashboard widgets
function remove_dashboard_meta() {
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
	remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
	remove_meta_box('dashboard_primary', 'dashboard', 'side');
	remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
	remove_meta_box('dashboard_activity', 'dashboard', 'normal');
}
add_action('admin_init', 'remove_dashboard_meta');




// Removing wp_head code injections
add_action('init', function() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
	remove_action('admin_print_styles', 'print_emoji_styles');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
	remove_filter('the_content_feed', 'wp_staticize_emoji');
	remove_filter('comment_text_rss', 'wp_staticize_emoji');

	if (function_exists('disable_emojicons_tinymce')) {
		add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce');
	}
});



// Custom dashboard widget

add_action('wp_dashboard_setup', function() {
	wp_add_dashboard_widget(
		'admin_widget',
		'CircleBack Lending Blog Settings',
		'admin_widget_display'
	);
});

function admin_widget_display() {
    include ('partials/admin_widget.php');
}




