<?php get_header(); ?>

<h1><?php echo get_the_archive_title(); ?></h1>

<?php
while(have_posts()):
the_post();
$featured_image = get_field('featured_image')['sizes']['large'];
?>


<div class="row">
	<div class="columns medium-4 four">
		<a href="<?php echo get_the_permalink(); ?>">
			<img src="<?php echo $featured_image; ?>" alt="" border="0" class="archive-image">
		</a>
	</div>
	<div class="columns medium-8 eight">
		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

		<div class="post-meta">
			Posted on <?php echo get_the_date(); ?><?php if(!is_category()) { ?> in <?php the_category(', '); } ?>
		</div>

		<?php echo get_excerpt_clipped($post->ID, 56); ?>

		<p align="right">
			<a href="<?php echo get_the_permalink(); ?>">Read more...</a>
		</p>
	</div>
</div><!-- .row -->

<br />
<br />

<?php
endwhile;
?>

<?php echo paginate_links(); ?>

<?php

get_footer();


