<?php

get_header();

while(have_posts()):
	the_post();
	$featured_image = get_field('featured_image')['sizes']['large'];
?>

<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
<div class="post-meta">Posted on <?php the_date(); ?> in <?php the_category(', '); ?></div>

<img src="<?php echo $featured_image; ?>" alt="" border="0" class="featured-image">

<?php the_content(); ?>

<br />
<br />

<?php
endwhile;

include('partials/related_articles.php');
get_footer();


