<?php
$current_cat_id = 0;

if (is_category()) {
	$current_cat_id = get_the_category()[0]->cat_ID;
} else if (is_archive() && is_month()) {
	$current_archive_month = get_the_date('F Y');
}

$categories = get_categories(array('orderby' => 'id'));
?>




<h3>Categories</h3>
<ul class="side-nav">
	<?php foreach($categories as $category) {
		$class = ($current_cat_id === $category->cat_ID) ? 'active' : '';

		echo '<li class="' . $class . '">';
		echo '<a href="' . get_category_link($category->cat_ID) . '">';
		echo $category->name;
		echo '</a></li>';
	} ?>
</ul>




<h3>Archives</h3>
<ul class="side-nav">
<?php
$archives = trim(wp_get_archives(['echo' => false, 'format' => 'custom', 'after' => '||']));
$archives = explode("||", str_replace(array("\n", "\t"), "", $archives));

array_pop($archives);

foreach ($archives as $archive) {
	$class = (isset($current_archive_month) && strstr($archive, $current_archive_month) !== FALSE) ? 'active' : '';

	echo '<li class="' . $class . '">' . $archive . '</li>';
}
?>
</ul>










