

        </div>
        <div class="columns medium-4 four show-for-medium-up">
            <?php include('sidebar.php'); ?>
        </div>
    </div><!-- .row -->
</div><!-- .wrapper -->



<footer>
    <div class="row">
        <div class="columns small-12 twelve">
            <hr />
        </div>
    </div><!-- .row -->

    <div class="row">
        <div class="columns small-12 twelve padleft">
            <s>&#10033;</s><b>Funding</b> &mdash; The loans are funded by County Bank of Rehoboth Beach, Delaware.  Member FDIC and an Equal Opportunity Lender.  The loan approval process may include receiving and reviewing borrower documents, and verifying the applicant’s identity, bank account, employment and/or income information.  Once approved, funds are typically disbursed the next business day.  In some circumstances, technical problems with Internet connectivity or the electronic funds transfer network may delay funds disbursal.
        </div>
    </div><!-- .row -->

    <br class="clearfix" />

    <div class="row">
        <div class="columns small-12 twelve padleft">
            <s>&#10070;</s><b>APR</b> &mdash; APRs range from 6.63% to 35.18% and reflect your interest rate and loan origination fee. Your APR may be higher due to a $5 monthly fee if you elect to make payments by any means other than automatic electronic transfers. Your actual rate depends upon credit score, loan amount, loan term, and credit usage and history.
        </div>
    </div><!-- .row -->

    <br class="clearfix" />

    <div class="row">
        <div class="columns small-12 twelve padleft">
            <s>&dagger;</s><b>Eligibility Criteria</b> &mdash; A borrower must be at least 18 years old; be a U.S. citizen or permanent resident alien in an eligible state (including Armed Forces Americas, Europe and Pacific); meet requirements such as income and credit thresholds; and provide a valid social security number, email address, U.S. bank account, and in some circumstances documentation verifying identity.<br /><br />You may be ineligible for this offer if: (1) you no longer meet our criteria when your application is processed; (2) you have already initiated or completed a loan application on the CircleBack Lending website; (3) you had or currently have a CircleBack Lending account; (4) your application is incomplete, inaccurate or unverifiable; (5) we received your application after the expiration date; or (6) you are outside the eligible jurisdictions in which the loans are offered.
        </div>
    </div><!-- .row -->

    <br class="clearfix" />

    <div class="row">
        <div class="columns small-12 twelve text-center">
            CircleBack Lending, Circlebacklending.com, and the CircleBack logo are trademarks or service marks of CircleBack Lending, Inc.  Copyright &copy; 2012-2015 CircleBack Lending, Inc.  All rights reserved.
        </div>
    </div><!-- .row -->
</footer>

<?php wp_footer(); ?>

</body>
</html>


