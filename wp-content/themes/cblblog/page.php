<?php
get_header();

while(have_posts()):
	the_post();
?>

<h1><?php the_title(); ?></h1>

<br />
<br />

<?php the_content(); ?>

<?php
endwhile;
get_footer();


