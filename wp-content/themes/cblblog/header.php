<!DOCTYPE html>
<html lang='en'>
<head>
	<title>CircleBack Lending</title>

	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
	<meta name='apple-mobile-web-app-capable' content='yes'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>

	<!--[if (gt IE 8)|!(IE)]><!-->
	<script src='<?php bloginfo('template_url'); ?>/public/js/jquery-2.1.4.min.js'></script>
	<!--<![endif]-->

	<!--[if lt IE 9]>
	<script src='<?php bloginfo('template_url'); ?>/public/js/jquery-1.9.1.min.js'></script>
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/public/css/foundation-3.2.5.min.css' />
	<![endif]-->

	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:600,400' />
	<link rel='stylesheet' href='<?php bloginfo('template_url'); ?>/public/css/style.min.css' />

	<script src='<?php bloginfo('template_url'); ?>/public/js/script.js'></script>

	<?php wp_head(); ?>
</head>

<!--[if (gt IE 9)|!(IE)]><!--><body><!--<![endif]-->
<!--[if lt IE 9]><body class="ie8"><![endif]-->


<div class="wrapper">
		
	<header>
		<div class="row">

			<div class="columns small-4 four">
				<a href="#" class="logo"></a>
			</div>

			<div class="columns small-8 eight text-right">

				<div class="show-for-small-only">
					<a href="#" class="hamburger"></a>
				</div>

				<nav role="navigation">
					<ul>
						<li><a href="#">Borrow</a>

							<ul>
								<li><a href="#">How to Borrow</a></li>
								<li><a href="#">Who Can Borrow?</a></li>
								<li><a href="#">Loan Rates</a></li>
								<li><a href="#">Borrower Fees</a></li>
							</ul>

						</li>
						<li><a href="#">Buy Loans</a></li>
						<li><a href="#">About Us</a>

							<ul>
								<li><a href="#">Overview</a></li>
								<li><a href="#">Management Team</a></li>
								<li><a href="#">Advisors</a></li>
								<li><a href="#">Press</a></li>
								<li><a href="#">Contact</a></li>
								<li><a href="#">Jobs</a></li>
							</ul>

						</li>
						<li><a href="#">Loan Types</a>

							<ul>
								<li><a href="#">Home Buying</a></li>
								<li><a href="#">Car Financing</a></li>
								<li><a href="#">Major Purchase</a></li>
							</ul>

						</li>
						<li><a href="#">Sign In</a></li>
					</ul>
				</nav>

			</div>
		</div><!-- .row -->
	</header>

	<div class="row">
		<div class="columns medium-4 four show-for-small-only sidebar-mobile">
            <?php include('sidebar.php'); ?>
        </div>

		<div class="columns medium-8 eight main-content">


