<?php
$related_posts = get_posts([
	'orderby' => 'rand',
	'category' => get_the_category()[0]->cat_ID,
	'post__not_in' => [$post->ID],
	'posts_per_page' => 3]);

if (count($related_posts)) {
	
	echo '<hr />';
	echo '<h2>Recommended ' . get_the_category()[0]->name . ' Articles</h2>';
	echo '<div class="row featured-articles">';

	foreach ($related_posts as $post) {
		setup_postdata($post);

		$featured_image = get_field('featured_image', $post->ID)['sizes']['large'];
		$end = (end($related_posts) == $post) ? ' end' : '';

		echo '<div class="columns small-4 four' . $end . '">';
		echo 	'<a href="' . get_the_permalink($post->ID) . '" class="featured-article-small"
					style="background-image: url(' . $featured_image . ')">';
		echo 		'<div class="overlay"></div>';
		echo 		'<h5>' . $post->post_title . '</h5>';
		echo 		'<p>' . get_excerpt_clipped($post->ID, 18) . '...</p>';
		echo 	'</a>';
		echo '</div>';
	}

	echo '</div><!-- .row -->';
}





