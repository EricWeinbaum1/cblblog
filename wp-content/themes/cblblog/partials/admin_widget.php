<?php
if (!empty($_POST)) {
	include ('../../../../wp-load.php');

	echo '{';

	foreach ($_POST as $k => $v) {
		$k = strip_tags($k);
		$v = strip_tags($v);


		if (update_option($k, $v)) {
			echo '{ "": }';
		}
	}

	echo '}';

	exit;
}




$category_options_string = '<option class="placeholder">Select...</option><option class="placeholder"></option>';
$categories = get_categories(array('orderby' => 'name'));

foreach($categories as $category) {
	$category_options_string .= '<option value="' . $category->cat_ID . '">' . $category->name . '</option>';
}

$posts_options_string = '<option class="placeholder">Select...</option><option class="placeholder"></option>';
$posts = get_posts(array('orderby' => 'name', 'posts_per_page' => -1));

foreach($posts as $post) {
	setup_postdata($post);
	$title = substr($post->post_title, 0, 50);
	$cat_id = (get_the_category($post->ID)[0]->term_id);

	$posts_options_string .= '<option class="hide" data-cat="' . $cat_id . '" value="' . $post->ID . '">' . $title . '</option>';
}

wp_reset_postdata();
?>



<form id="cbl_settings_form">
	<table width="99%" border="0" cellspacing="0" cellpadding="5" border="0" class="cbl_settings">
		<tr>
			<td width="200"><b>Featured article</b></td>

			<td>
				<select id="cbl_opt_featured_article_cat" name="cbl_opt_featured_article_cat">
					<?php echo $category_options_string; ?>
				</select>

				<select id="cbl_opt_featured_article" name="cbl_opt_featured_article" disabled>
					<?php echo $posts_options_string; ?>
				</select>
			</td>
		</tr>
	</table>
</form>

<br />

<a class="button button-primary button-large right" id="cbl_settings_submit">Submit</a>

<div class="clearfix"></div>






<style type="text/css">
.cbl_settings select {
	max-width: 49.999%;
}

.cbl_settings select .hide {
	display: none;
}
</style>



<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#cbl_opt_featured_article_cat').on('change', function() {
		var id = $(this).val();

		$('#cbl_opt_featured_article').attr('disabled', false);
		$('#cbl_opt_featured_article option').not('.placeholder').addClass('hide');
		$('#cbl_opt_featured_article option[data-cat="' + id + '"]').removeClass('hide');
	});

	$('#cbl_settings_submit').on('click', function(e) {
		e.preventDefault();

		var form_data = $('#cbl_settings_form').serialize();

		$.post('<?php bloginfo('template_url'); ?>/partials/admin_widget.php', form_data, function(data) {
			console.log(data);
		});
	});
});
</script>









