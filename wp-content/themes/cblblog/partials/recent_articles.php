<?php

echo '<h2>Recent Posts</h2>';
$recent_posts = get_posts(['orderby' => 'date', 'posts_per_page' => 5]);

echo '<ul>';

foreach ($recent_posts as $post) {
	setup_postdata($post);
	echo '<li>';
	echo 	'<a href="' . get_the_permalink() . '">' . get_the_date() . ' &mdash; ' . get_the_title() . '</a>';
	echo '</li>';
}

echo '</ul>';

wp_reset_postdata();



