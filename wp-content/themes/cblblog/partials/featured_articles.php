<?php
global $post;



/* Featured articles */
$category_ids = [1, 2, 3, 4, 5, 6];
$boxes_per_row = 3;




/* Large/focused */
$id = 5; // Temp category ID

$category_name = get_cat_name($id);
$category_post = get_posts(['orderby' => 'date', 'category' => $id, 'nopaging' => true, 'posts_per_page' => 1]);
$latest_post = $category_post[0];
setup_postdata($latest_post);

$featured_image = get_field('featured_image', $latest_post->ID)['sizes']['large'];

echo '<div class="row">';
echo 	'<div class="columns small-12 twelve">';

echo 		'<a href="' . get_the_permalink($latest_post->ID) . '"
			class="featured-article-large"
			style="background-image: url(' . $featured_image . ')">';

echo 			'<div class="table-wrap">';
echo 				'<div class="table-wrap-inner">';
echo 					'<div class="overlay">';
echo 						'<h5>' . $latest_post->post_title . '</h5>';
echo 						'<p>' . get_excerpt_clipped($latest_post->ID, 45) . '</p>';
echo 					'</div>';
echo 				'</div>';
echo 			'</div>';

echo 		'</a>';

echo    	'<label class="category-link">' . $category_name . '</label>';
echo 	'</div>';
echo '</div><!-- .row -->';

wp_reset_postdata();




/* Small */
echo '<div class="row featured-articles">';
foreach ($category_ids as $index => $id) {
	$category_name = get_cat_name($id);

	$category_post = get_posts(['orderby' => 'date', 'category' => $id, 'nopaging' => true, 'posts_per_page' => 1]);
	$latest_post = $category_post[0];
	setup_postdata($latest_post);

	$featured_image = get_field('featured_image', $latest_post->ID)['sizes']['large'];

	if ($index%$boxes_per_row === 0 && $index > 0) {
		echo '</div><!-- .row -->';
		echo '<div class="row">';
	}

	echo '<div class="columns small-4 four">';
	echo 	'<a href="' . get_the_permalink($latest_post->ID) . '"
				class="featured-article-small"
				style="background-image: url(' . $featured_image . ')">';
	echo 		'<div class="overlay"></div>';

	echo 		'<div class="table-wrap">';
	echo 			'<div class="table-wrap-inner">';
	echo 	 			'<h5>' . $latest_post->post_title . '</h5>';
	echo 	 			'<p>' . get_excerpt_clipped($latest_post->ID, 20) . '...</p>';
	echo 			'</div>';
	echo 		'</div>';
	echo 	'</a>';

	echo    '<label class="category-link">' . $category_name . '</label>';

	echo '</div>';
}

echo '</div><!-- .row -->';

wp_reset_postdata();










