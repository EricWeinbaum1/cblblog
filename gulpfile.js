var gulp = require('gulp'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	notify = require('gulp-notify'),
	sourcemaps = require('gulp-sourcemaps');

var theme_path = 'wp-content/themes/cblblog/';

gulp.task('build-css', function() {
    gulp.src(theme_path + 'sass/main.scss')
    	.pipe(sourcemaps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write())
        .pipe(notify({ message: 'CSS build successful', title: 'Build results', wait: true }))
        .pipe(gulp.dest(theme_path + 'public/css/'))
});

gulp.task('default', function() {
    gulp.watch(theme_path + 'sass/*.scss', ['build-css']);
    gulp.start('build-css');
});





